﻿
class Email {
    constructor(from, to, subject, body, template) {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.body = body;
        this.template = template;
    };
};

class Party {
    constructor(fullname, email) {
        this.fullname = fullname;
        this.email = email;
    };
};

class Template {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    };
};

var emails = [];
listItem([]);

form = document.getElementById("form");
btn_add = document.getElementById("btn_add");
btn_send = document.getElementById("btn_send");
emails_count = document.getElementById("emails_count");

function addEmails(event) {
    event.preventDefault();

    sender_email = document.getElementById("sender_email").value;
    sender_fullname = document.getElementById("sender_fullname").value;
    receiver_email = document.getElementById("receiver_email").value;
    receiver_fullname = document.getElementById("receiver_fullname").value;
    subject = document.getElementById("subject").value;
    message = document.getElementById("message").value;
    templateObj = document.getElementById("templates").value;
    template = JSON.parse(templateObj);

    file = document.getElementById("file");

    console.log(file.value);

    var sender = new Party(sender_fullname, sender_email);
    var receiver = new Party(receiver_fullname, receiver_email);
    var template = new Template(template.id, template.name);
    var email = new Email(sender, receiver, subject, message, template);

    if (sender.email !== "" && receiver.email !== "") {
        emails.push(email);
        listItem(emails);
        emails_count.innerHTML = emails.length;
    }
}

function sendEmails(emails) {
    if (emails.length > 0) {
        $.ajax({
            url: '/Home/SendgridEmailSubmit',
            data: JSON.stringify(emails),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
        });
    }

    emails = [];
    listItem([]);
    emails_count.innerHTML = emails.length;
}

function listItem(list) {
    if (list.length == 0) {
        var message = '<p class="placeholder-msg">No emails were created..</p>'
        document.getElementById("email_list").innerHTML = message;
    }
    else {
        var element = '<ul>'
        list.forEach(function (item) {
            element +=
                `<li>
                    <h4>${item.to.fullname}</h4>
                    <h6>${item.to.email}</h6>
                </li>`;
        });
        element += '</ul>';
        document.getElementById("email_list").innerHTML = element;
    }
}

form.addEventListener('submit', addEmails);
btn_send.addEventListener('click', () => sendEmails(emails));
