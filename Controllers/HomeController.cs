﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mailApp.Models;
using SendGrid;
using SendGrid.Helpers.Mail;
using Newtonsoft.Json;
using System.Text;
using System.IO;

namespace mailApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public async Task<IActionResult> SendgridEmailSubmit([FromBody] List<EmailModel> emailModels)
        {

            ViewData["Message"] = "Email Sent!!!...";
            await Execute(emailModels);
            return View("Index");
        }

        static async Task Execute(List<EmailModel> emailModels)
        {
            var apiKey = Environment.GetEnvironmentVariable("SENDGRID_API_KEY");
            var client = new SendGridClient(apiKey);

            foreach (var emailModel in emailModels)
            {
                var mail = new SendGridMessage
                {
                    From = new EmailAddress(emailModel.From.Email),
                    TemplateId = emailModel.Template.Id,
                    Personalizations = new List<Personalization>()
                };

                //var file = Directory.GetFiles(@"D:\Desktop\reddmail-public\wwwroot\Attachments").FirstOrDefault();
                //var bytes = System.IO.File.ReadAllBytes(file);
                //var attachment = Convert.ToBase64String(bytes);
                //mail.AddAttachment("Test.pdf", attachment);

                //ByteArrayOutputStream pdfBytes = createPdf();
                //String encodedPdf = new String(Base64.getEncoder().encode(pdfBytes.toByteArray()));

                var filePath = @"C:\Users\DELL\Desktop\Test.txt";
                byte[] byteData = Encoding.ASCII.GetBytes(System.IO.File.ReadAllText(filePath));
                mail.Attachments = new List<Attachment>
                {
                    new Attachment
                    {
                        Content = Convert.ToBase64String(byteData),
                        Filename = "Test.txt",
                        Type = "text/plain",
                        Disposition = "attachment",
                        ContentId = "Test.txt"
                    }
                };

                mail.Personalizations.Add(
                    new Personalization()
                    {
                        Tos = new List<EmailAddress>
                        {
                            new EmailAddress(emailModel.To.Email)
                        },
                        TemplateData = new TemplateData
                        {
                            Full_Name = emailModel.To.FullName,
                            Email = emailModel.To.Email,
                            Subject = emailModel.Subject,
                            Body = emailModel.Body
                        }
                    });
                var response = await client.SendEmailAsync(mail);
            }

        }

        public class TemplateData
        {
            public string Full_Name { get; set; }
            public string Email { get; set; }
            public string Subject { get; set; }
            public string Body { get; set; }

        }

    }
}
