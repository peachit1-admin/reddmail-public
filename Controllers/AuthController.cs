﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace mailApp.Controllers
{
    public class AuthController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FormSubmit()
        {
            var response = Request.Form["g-recaptcha-response"];
            var secretKey = "6LfI3gwbAAAAABudV2eHrLSkRh8chtC5ffc0ANRE";
            var client = new WebClient();
            var result = client.DownloadString(
                        string.Format(
                            "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}",
                            secretKey,
                            response
                        ));

            var obj = JObject.Parse(result);
            var status = (bool)obj.SelectToken("success");
            ViewData["Message"] = status ? "Success" : "Failed";

            return View("Index");
        }

    }
}
