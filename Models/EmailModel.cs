﻿using System;

namespace mailApp.Models
{
    public class EmailModel
    {
        public Party From { get; set; }
        public Party To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public Template Template { get; set; }
    }

    public class Party
    {
        public string FullName { get; set; }
        public string Email { get; set; }
    }

    public class Template 
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

}
